import { LightningElement, api, track, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { CloseActionScreenEvent } from 'lightning/actions';
import Inventory from '@salesforce/schema/Inventory__c';
import { NavigationMixin } from 'lightning/navigation';
import { getRecord } from 'lightning/uiRecordApi'
const FIELDS = ['Inventory__c.Product_Family__c', 'Inventory__c.Product_Type__c', 'Inventory__c.Type_of_purchase__c'];
import saveinv from '@salesforce/apex/AssetController.getinvdet';
import id from '@salesforce/user/Id';
export default class Inventorylwc extends NavigationMixin(LightningElement) {

    @api recordId;
    hardware = false;
    software = false;
    laptop = false;

    perpetual = false;
    accordianSection = ['product', 'warrant', 'soft', 'Hard'];
    richtext = "<h2>Not covered under warranty when Expired </h2>";

    @track showSpinner = true;
    @track record;
    @track error;
    @track expdate;
    @track retired=false;
    @track service=false;
    @track warranclaimed=false;
    @track serdisabled=false;
    productfmchg(event) {
        if (event.detail.value == 'Hardware') {
            this.hardware = true;
            this.software = false;
        }
        else {
            this.hardware = false;
            this.software = true;
        }
    }

    purchtype(event) {
        console.log('test' +event.detail.value);
        if (event.detail.value != 'Perpetual' &&  event.detail.value !=null ) {
            this.perpetual = true;
        }
        else {
            this.perpetual = false;
        }
    }
    handlecalen(event)
    {
        this.expdate=event.detail.value;
    }

    retirehandle(event)
    {
        console.log('event ' + event.detail.checked)
    if (event.detail.checked == true) {
        console.log('event ' + event.detail.checked)
        this.retired = true;
    }
    else
    {
        this.retired = false;
    }

    }
    servicehandle(event)
    {
        if (event.detail.checked == true) {
            console.log('event ' + event.detail.checked)
            this.service = true;
        }
        else
        {
            this.service = false;
        }
    }

    handleclaimed(event)
    {
        if (event.detail.checked == true) {
            this.warranclaimed = true;
        }
        else
        {
            this.warranclaimed = false;
        }
    }

    prodchangehandler(event) {
        if (event.detail.value == 'Laptop') {
            this.laptop = true;
        } else {
            this.laptop = false;
        }
        /*if (event.detail.value == 'Laptop') {
            this.hardware = true;
            this.software = false;
            this.laptop = true;
        }
        else {
            this.hardware = false;
            this.software = true;
            this.laptop = false;
        } */
        this.hardware = true;
        this.software = false;
        //this.laptop = true;
    }

    handleToggleSection(event) {


    }

    renderedCallback() {
        const inputFields = this.template.querySelectorAll(
            'lightning-input-field'
        );
        if (inputFields) {
            inputFields.forEach(field => {
                if (field.fieldName == 'Name')
                    console.log('val' + field.value);

            });
        }
    }
    connectedCallback() {
        if (this.recordId != undefined) {
            saveinv({ invid: this.recordId })
                .then(result => {
                    console.log('tet1' + result[0].Product_Family );
                    if (result != null) {
                        if (result[0].Product_Family == 'Hardware') {
                            this.hardware = true;
                            this.software = false;
                            if (result[0].Product_Type == 'Laptop') {
                                this.laptop = true;
                            }
                            else {
                                this.laptop = false;
                            }

                        }
                        else {
                            this.hardware = false;
                            this.software = true;
                        }
                        if (result[0].Type_of_purchase != 'Perpetual' && result[0].Type_of_purchase !=null ) {
                            this.perpetual = true;
                        }
                        else {
                            this.perpetual = false;;
                        }
                        if (result[0].service ==true && result[0].service !=null ) {
                            this.serdisabled = true;
                        }
                        else {
                            this.serdisabled = false;;
                        }
                        


                        if (result[0].Renewal_Warranty_expirydate__c) {
                            var today = new Date();
                            today.setHours(0,0,0,0);
                            if(new Date(result[0].Renewal_Warranty_expirydate__c) < today) {
                                console.log('Satisfies the condition');
                                const toastEvent = new ShowToastEvent({
                                    title: 'Warranty Expired !',
                                    variant: 'error',
                                    message: 'Not covered under warranty when Expired !',
                                });
                                this.dispatchEvent(toastEvent);
                            }
                        }
                    }
                    this.showSpinner = false;

                }).catch(error => {
                    this.showSpinner = false;
                })
        }
        else {
            this.showSpinner = false;
        }

    }

    handleSubmit(event) {
        this.showSpinner = true;
    }
    testload() {
        const inputFields = this.template.querySelectorAll('lightning-input-field');
    }
    handleSuccess(event) {
        var isVal = true;
        this.template.querySelectorAll('lightning-input-field').forEach(element => {
            isVal = isVal && element.reportValidity();
        });
        if (isVal) {
        this.showSpinner = false;
        const updatedRecord = event.detail.id;
        const toastEvent = new ShowToastEvent({
            title: 'Record Updated !',
            variant: 'success',
            message: 'Record updated successfully !',
        });
        this.dispatchEvent(toastEvent);
        this.dispatchEvent(new CloseActionScreenEvent());
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: this.recordId == undefined ? updatedRecord : this.recordId,
                objectApiName: 'Inventory__c',
                actionName: 'view'

            },
        });
    }
    else{
        this.dispatchEvent(
            new ShowToastEvent({
                title: 'Error creating record',
                message: 'Please enter all the required fields',
                variant: 'error',
                }),
            );
    }
    }

    handleError(event) {
        this.showSpinner = false; const toastEvent = new ShowToastEvent({
            title: 'Error !',
            variant: 'error',
            message: event.detail.detail,
        });
        this.dispatchEvent(toastEvent);
    }

    closeModal(event) {
        this.dispatchEvent(new CloseActionScreenEvent());
        this[NavigationMixin.Navigate]({
            type: 'standard__objectPage',
            attributes: {
                objectApiName: 'Inventory__c',
                actionName: 'home',
            },
        });



    }

}