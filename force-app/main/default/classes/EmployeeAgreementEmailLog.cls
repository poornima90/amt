global class EmployeeAgreementEmailLog implements Messaging.InboundEmailHandler{
    
  global Messaging.InboundEmailResult handleInboundEmail(Messaging.inboundEmail email, 
                                                       Messaging.InboundEnvelope env){
                                                           
    Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
   
    Task[] newTask = new Task[0];
 
         String emailSubject = email.subject;
          String assetID = emailSubject.Substring(emailSubject.indexof('REF-')+4,emailSubject.indexof('AMT'));
                                                           String body = email.htmlBody;
                                                           body = body.toLowerCase();
                Asset asset = [select id,name,Agreement_Accepted__c from Asset where id=:assetID];       
              if( asset!=null ){
                EmailMessage emailMessage = new EmailMessage();
                emailMessage.status = '3';
                emailMessage.relatedToId = asset.id;
                emailMessage.fromAddress = email.fromAddress;
                emailMessage.FromName = email.fromName;
                emailMessage.subject = email.subject; // email subject
                emailMessage.htmlBody = email.htmlBody;
                emailMessage.textbody =email.plainTextBody;
                  system.debug('email.htmlBody'+email.htmlBody+'email.plainTextBody'+email.plainTextBody);
                insert EmailMessage;            
                  if(body.contains('accepted') && !emailSubject.contains('AMT##')){
                      asset.Agreement_Accepted__c = true;
                      update asset;
                  }
                 }
   result.success = true;
   
   return result;
  }
}