import lookUp from '@salesforce/apex/lookupcontroller.search';
import { api, LightningElement, track, wire } from 'lwc';


export default class customLookUp extends LightningElement {

    @api objName;
    @api iconName;
    @api filter = '';
    @api prodid;
    @api searchPlaceholder = 'Search';
    @api isReadOnly;
    @track selectedName;
    @track records;
    @track isValueSelected;
    @track blurTimeout;
    @api filtercrit;
    searchTerm;
    @track isemp = true;
    @track _selnme;
    //css
    @track boxClass = 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click slds-has-focus';
    @track inputClass = '';
   
    seltnme;
    @api
    get selname() {
        return this._selnme;
    }
    set selname(value) {
        this.setAttribute('selname', value);
        this._selnme = value;
        if (this._selnme == '') {
            this.isValueSelected = false;
            const valueSelectedEvent = new CustomEvent('clearval', { detail: 'clear ' + this.objName });
            this.dispatchEvent(valueSelectedEvent);
        }
    }

    // set selname(value) {
    //     console.log('selname1 ' + this.selectedName);
    //     if(this.selectedName=='' || this.selectedName==null ||  this.selectedName==undefined)
    //     {
    //         this.seltnme = value;
    //         console.log('selname ' + this.selname);

    //         this.selectedName =  this.seltnme;
    //         console.log('selectedName ' + this.selectedName);
    //     }

    // }
    connectedCallback() {
        
        /* wiredRecords({ error, data }) {
            if (data) {
                this.error = undefined;
                this.records = data;
            } else if (error) {
                this.error = error;
                this.records = undefined;
            }
        } */

        if (this.objName == 'Inventory__c') {
            this.isemp = false;
        }
    }

    handleClick() {
        this.searchTerm = '';
        this.inputClass = 'slds-has-focus';
        this.boxClass = 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click slds-has-focus slds-is-open';

        lookUp({searchTerm: this.searchTerm, myObject: this.objName, filter: this.filter})
        .then(result => {
            console.log('result -->',JSON.stringify(result))
            this.records = result;
        })
        .catch(error => {
            this.error = error;
        })
    }

    onBlur() {
        this.blurTimeout = setTimeout(() => { this.boxClass = 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click slds-has-focus' }, 300);
    }

    onSelect(event) {
        let selectedId = event.currentTarget.dataset.id;
        let selectedName = ''
        if (this.objName == 'Inventory__c') {
            this.isemp = false;
            selectedName = event.currentTarget.dataset.name;
        }
        else {
            selectedName = event.currentTarget.dataset.name;
        }

        let val = this.records.find(data => data.Id === selectedId);
        // var empdet={id:selectedId,Name:selectedName,empdiv: event.currentTarget.dataset.Business_Division__c,desig:event.currentTarget.dataset.Designation__c,amt:event.currentTarget.dataset.AMT_Employee_Type__c,num:event.currentTarget.dataset.Unique_Employee_No__c}   ;   
        // console.log('lookup ' +  empdet);

        const valueSelectedEvent = new CustomEvent('lookupselected', { detail: val });
        this.dispatchEvent(valueSelectedEvent);
        this.isValueSelected = true;
        this.selectedName = selectedName;
        if (this.blurTimeout) {
            clearTimeout(this.blurTimeout);
        }
        this.boxClass = 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click slds-has-focus';
    }

    handleRemovePill() {
        this.isValueSelected = false;

        const valueSelectedEvent = new CustomEvent('removepill', { detail: this.objName });
        this.dispatchEvent(valueSelectedEvent);
    }

    onChange(event) {
        this.searchTerm = event.target.value;

        lookUp({searchTerm: this.searchTerm, myObject: this.objName, filter: this.filter})
        .then(result => {
            this.records = result;
        })
        .catch(error => {
            this.error = error;
        })
    }



}