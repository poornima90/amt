trigger AssetTrigger on Asset (before insert,after insert,before update){
    if(Trigger.isInsert){        
         
        if(Trigger.isBefore){
            for(Asset asset : Trigger.New){
            String name = asset.Product_Family__c;
            if(asset.Inventory_Product_Type__c != null)
                name =  name+' '+asset.Inventory_Product_Type__c+ ' ' + DateTime.now();
            else
                name =  name+' ' + DateTime.now();
            asset.Name = name;
        }
        }
        else{
        List<id> inventoryIds = new List<id>();
        for(Asset asset : Trigger.New){
            inventoryIds.add(asset.Product_Name__c);
        }
        List<Inventory__c> inventoryList =  [select id,Assignedunassigned__c,Product_Family__c,Assigned_License_Count__c,UnAssigned_License_Count__c from Inventory__c where id IN :inventoryIds];
        
        for(Inventory__c inventory : inventoryList){
            if(inventory.Product_Family__c == 'Hardware'){
                inventory.Assignedunassigned__c = true;
            }
            else if(inventory.Product_Family__c == 'Software' )
            {
                Decimal count = inventory.UnAssigned_License_Count__c -1;
                inventory.UnAssigned_License_Count__c = count;
                inventory.Assigned_License_Count__c = inventory.Assigned_License_Count__c+1;
                if(count < 1)
                {
                    inventory.Assignedunassigned__c = true;
                }
                
            }
            
        }
        update inventoryList;
        }
    }
    if(Trigger.isUpdate){
        List<id> inventoryIds = new List<id>();
        for(Asset asset : Trigger.New){
            
            Asset oldasset = Trigger.oldMap.get(asset.Id);
            Boolean oldAssetActive = oldasset.Active__c;
            Boolean newAssetActive = asset.Active__c;
            if(oldAssetActive && !newAssetActive){
                inventoryIds.add(asset.Product_Name__c);
            }
            
        }
        if(inventoryIds.size()>0){
            List<Inventory__c> inventoryList =  [select id,Assignedunassigned__c,Product_Family__c,Assigned_License_Count__c,UnAssigned_License_Count__c from Inventory__c where id IN: inventoryIds];
            for(Inventory__c inventory : inventoryList){
                if(inventory.Product_Family__c == 'Hardware' ){
                    inventory.Assignedunassigned__c = false;
                }
                else if(inventory.Product_Family__c == 'Software')
                {
                    inventory.UnAssigned_License_Count__c = inventory.UnAssigned_License_Count__c + 1;
                    inventory.Assigned_License_Count__c = inventory.Assigned_License_Count__c - 1;
                    inventory.Assignedunassigned__c = false;
                }
                
            }
            update inventoryList;
        }
    }
}