public with sharing class  AssetController {
  @AuraEnabled(cacheable=true)
     public static List<Asset> getassest(string assetid) {
      List<Asset> ast;
          system.debug('asts ' + assetid);    
         ast =  [SELECT Name,Employee_Name__c,Product_Name__c,SerialNumber,Product_Family__c,Product_Code__c,Brand__c,Active__c,Employee_Business_Division__c,AMT_Employee_Designation__c,Employee_No__c,AMT_Employee_Type__c,EmployeeName__c,productName__c,Inventory_Product_Type__c,Employee_Location__c,Employee_Country__c,Product_Information__c,Sedin_Asset_ID__c from Asset where id=:assetid ];


    system.debug('ast ' + ast);    
    return ast;
  
     
     }
    
    @AuraEnabled(cacheable=true)
     public static List<sObject> getchidassest(string invid) {
      List<Asset> ast;
        
         //set<id> masterid = new set<id>();
          system.debug('asts ' + invid);    
         ast =  [ Select Id,Name, productName__c,Product_Name__c,SerialNumber,Product_Family__c,Product_Code__c,Brand__c,Active__c,Sedin_Asset_ID__c from Asset where Parent_Product__c=:invid and Active__c=true ];
         
       /*  for (Asset asd : ast)
		{
   			 masterid.add(asd.Product_Name__c);
		}
         List<Inventory__c> invpro =  [Select Id, Product_Name__c,Serial_Number__c,Product_Family__c,Product_Code__c,Brand__c,Active__c,Asset_ID__c,Information__c from  Inventory__c where Id in: masterid ];*/
         
         
    system.debug('ast ' + ast);    
    return ast;
  
     
     }
    
    @AuraEnabled
     public static List<string> saveasset(Asset ast,boolean isnew,list<Inventory__c> child,list<string> delchild) {
         system.debug('ast ' +ast );
         system.debug('isnew ' +isnew );
         system.debug('child ' +child );
      List<string> result = new List<string>(); 
         try{
              if(isnew==true)
          {
              Asset asst  = new Asset();
              asst.Name=  ast.Name; // 'testforone -'+ ast.Product_Name__c +  ast.Employee_Name__c + system.Datetime.now();
              asst.Employee_Name__c= ast.Employee_Name__c;
              asst.Product_Name__c = ast.Product_Name__c;
              asst.Inventory__c = ast.Product_Name__c;
              asst.SerialNumber= ast.SerialNumber;
              //asst.ProductCode= ast.ProductCode;
              asst.Brand__c= ast.Brand__c;
              asst.Active__c= ast.Active__c;
              insert asst;
             string res = addchildasset(ast.Product_Name__c,isnew,child,delchild);
            result.add('res')  ;
             result.add(asst.id)  ; 
          }
           else
           {
              Asset asst  = new Asset();
             // asst.Name='testforone -'+ asst.Product_Name__c ;
             asst.id=ast.id;
              /* asst.Employee_Name__c= ast.Employee_Name__c;
              asst.Product_Name__c = ast.Product_Name__c;
             asst.SerialNumber= ast.SerialNumber;
              //asst.ProductCode= ast.ProductCode;
              asst.Brand__c= ast.Brand__c;*/
              asst.Active__c= ast.Active__c;
              update asst;
             string res = addchildasset(ast.Product_Name__c,isnew,child,delchild);
          	result.add(res)  ;
             result.add(asst.id)  ; 
             
           }
         }
        
catch(Exception  e)
{
    result.add(e.getMessage());	
}
   
    return result;
  
 }
   @AuraEnabled
    public static string addchildasset(string ast,boolean isnew,list<Inventory__c> child,list<string> delchild)
    {
        string result;
        try{
            
   if(child!=null && child.size()>0)
               	
   {
                   for (Inventory__c str : child)
                   {
                         Asset chdasst  = new Asset();
                       	 chdasst.Product_Name__c = str.id;
              			 chdasst.Inventory__c = str.id;
                      	 chdasst.Parent_Product__c = ast;
                         chdasst.SerialNumber= str.Serial_Number__c;
              			 chdasst.Brand__c= str.Brand__c;
                      	 chdasst.Active__c= true;
                         insert chdasst;
               
                   }
                  
                
               }
         			if(delchild!=null && delchild.size()>0)
               	
  					 {
  	                 for (String str : delchild)
                   {
  						Asset asst  = new Asset();
                       asst.id=str;
                       asst.Active__c=false;
                   	 update asst ;
                   }
               
        }
            result='success';   
        }
     catch(Exception  e)
{
    result = e.getMessage();	
}
        return result;
    }
    
    
    @AuraEnabled(cacheable=true)
     public static List<inventorylwcobj> getinvdet(string invid) {
         List<inventorylwcobj> lstlwc = new  List<inventorylwcobj>();
      system.debug('invid ' + invid);  
      	List<Inventory__c> ast =  [SELECT id,Product_Family__c,Product_Type__c,Type_of_purchase__c from Inventory__c where id=: invid];
         List<AggregateResult> result   = [SELECT COUNT(Id) total FROM Asset WHERE Inventory__c =: invid and Active__c	= true];
         system.debug('asts ' + ast);  
 		inventorylwcobj obj1 = new inventorylwcobj();
           system.debug('asts ' + invid);
         for (Inventory__c obj : ast) {
         	obj1.invid=	obj.id;
       		 obj1.Product_Family=  obj.Product_Family__c;
        	 obj1.Product_Type= obj.Product_Type__c;
         	obj1.Type_of_purchase=  obj.Type_of_purchase__c;
             if(result !=null && result.size() >0 && result[0].get('total')!=null &&  (Integer)result[0].get('total') >=1)
             {
                 obj1.service=true;
             }
             else
             {
                 obj1.service=false;
             }    
   			lstlwc.add(obj1);
         }


    system.debug('ast ' + lstlwc);    
    return lstlwc;
  
     
     }
   
    @AuraEnabled(cacheable=true)
     public static List<Inventory__c> getexppirylst() {
      List<Inventory__c> ast;
         
          //system.debug('asts ' + invid);    
         ast =  [SELECT id,Product_Name__c,name,Renewal_Warranty_expirydate__c from Inventory__c where Renewal_Warranty_expirydate__c >= TODAY  And Renewal_Warranty_expirydate__c <= NEXT_N_DAYS:5 ];


    system.debug('ast ' + ast);    
    return ast;
  
     
     }
    
    public class  inventorylwcobj {
          @AuraEnabled
       public  id invid { get; set; }
          @AuraEnabled
       public  string Product_Family { get; set; }
          @AuraEnabled
       public 	string Product_Type { get; set; }
          @AuraEnabled
      public   string Type_of_purchase { get; set; }
          @AuraEnabled
     public    boolean service { get; set; }
    }
}