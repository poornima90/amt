import { LightningElement,track,api,wire } from 'lwc';
import lookUp from '@salesforce/apex/lookupcontroller.search';
import { getRecord } from 'lightning/uiRecordApi';
import getassest from '@salesforce/apex/AssetController.getchidassest';
import saveasset from '@salesforce/apex/AssetController.addchildasset';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
const FIELDS = ['Asset.Inventory__c', 'Asset.Id'];


export default class Childassetlwc extends LightningElement {

    @track isModalOpen = false;
    @track records;
    @track Assrec;
    @track error;
    @track childRecords = [];
    @track newrecords = [];
    @track addrecords=[];
    @track delrecords=[];
    @api isinventory=false;
    @api recordId;
    @api masterId;
    @track showSpinner=false;
    @api
    get modalopen() {
        console.log('enter get');
        return this.isModalOpen;
    }
    set modalopen(value) {
        console.log('enter');
        this.setAttribute('modalopen', value);
        this.isModalOpen = value;
        console.log('enter ' + this.isModalOpen);
    }

    @wire(getRecord, { recordId: '$recordId', fields: FIELDS })
    wiredRecord({ error, data }) {
        console.log('error1 ' + error);
        console.log('result1' +  data);
        if (error) {
            console.log('is ' + this.isinventory);
            if(this.isinventory=='true')
            { console.log('is s' + this.isinventory);
                this.masterId= this.recordId;
                console.log('isaa1 ' + this.isinventory);
                //this.getchildassets();
            }
            else
            {
                alert('error1s');
            }
           
        }
        else if (data) {
         
            
            this.masterId= data.fields.Inventory__c.value;
        }
            
            
           // this.getchildassets();
            
        
    }

    @wire(getassest,{invid:'$masterId'})
    wiredrecode3({ error, data }) {
        console.log('error ' + error);
        console.log('result' +  data);
        if (error) {
            console.log('err');
            this.error = 'Unknown error';

            if (Array.isArray(error.body)) {
                this.error = error.body.map(e => e.message).join(', ');
            } else if (typeof error.body.message === 'string') {
                this.error = error.body.message;
            }
           
        }
        else if (data) {
            console.log('ass ' + data);
            this.Assrec = data;
                this.error = undefined;
                if (this.Assrec != undefined) {

                    let rows = JSON.parse( JSON.stringify( this.Assrec ) );
                    for ( let i = 0; i < rows.length; i++ ) {  

                        let childval = rows[ i ];
                       // dataParse.OwnerName = dataParse.Owner.Name;
                       childval.isnew = false;
                    }

                        this.addrecords=rows;
                       
                

               
                }
            
           // this.getchildassets();
            
        }
    }
    

        // getchildassets()
        // {
        //     console.log('test' );
        //     getassest({ invid: this.masterId })
        //     .then(result => {
        //         this.Assrec = result;
        //         this.error = undefined;
        //         if (this.Assrec != undefined) {

        //             let rows = JSON.parse( JSON.stringify( this.Assrec ) );
        //             for ( let i = 0; i < rows.length; i++ ) {  

        //                 let childval = rows[ i ];
        //                // dataParse.OwnerName = dataParse.Owner.Name;
        //                childval.isnew = false;
        //             }

        //                 this.addrecords=rows;
                       
                

               
        //         }
                
        //     }).catch(error => {
        //         this.error = 'Unknown error';

        //         if (Array.isArray(error.body)) {
        //             this.error = error.body.map(e => e.message).join(', ');
        //         } else if (typeof error.body.message === 'string') {
        //             this.error = error.body.message;
        //         }
        //     })
        // }

    openModal() {
        // to open modal set isModalOpen tarck value as true
        this.modalopen = true;
    }
    closeModal() {
        // to close modal set isModalOpen tarck value as false
        this.modalopen = false;
        this.oncloseevent();
       
    }
    submitDetails() {
        // to close modal set isModalOpen tarck value as false
        //Add your code to call apex method or do some processing
        
        var val = [];
        val.push(this.newrecords);
        val.push(this.delrecords);
        if(this.isinventory=='false')
        {
            this.modalopen = false;
            const valueSelectedEvent = new CustomEvent('onsave', { detail: val });
            this.dispatchEvent(valueSelectedEvent);
        }
        else
        {
            let del=[];
            if( this.delrecords !=null  && this.delrecords!=undefined && this.delrecords.length>0 )
            {
               
                for ( let i = 0; i < this.delrecords.length; i++ ) {  
                    //console.log('enter5' + this.addrecords[1][i].Id);
                    del.push(this.delrecords[i].Id);
                    console.log('enter3' + del);
                }
            }
            this.showSpinner=true;
                    saveasset({ ast: this.masterId, isnew: true ,child: this.newrecords,delchild : del })
            .then(result => {
                if (result == 'success') {
                   // this.recordId = result[1];
                    // this.showSpinner=false;
                    this.dispatchEvent(
                        new ShowToastEvent({
                            title: 'Success',
                            message: 'Child Asset Updated',
                            variant: 'success',
                        }),
                    );
                    this.showSpinner=false;
                   // window.location.reload();
                    this.modalopen = false;
                    const valueSelectedEvent = new CustomEvent('onsave', { detail: val });
                    this.dispatchEvent(valueSelectedEvent);
                    // this[NavigationMixin.Navigate]({
                    //     type: 'standard__recordPage',
                    //     attributes: {
                    //         recordId: this.recordId,
                    //         objectApiName: 'Inventory__c',
                    //         actionName: 'view'

                    //     },
                    // });

                }
                else {
                    this.dispatchEvent(
                        new ShowToastEvent({
                            title: 'Error',
                            message: result,
                            variant: 'error',
                            mode: 'dismissable'

                        }),
                    );
                    this.modalopen = false;
                    const valueSelectedEvent = new CustomEvent('onsave', { detail: val });
                    this.dispatchEvent(valueSelectedEvent);
                }


            }).catch(error => {
                this.showSpinner = false;
                this.error = 'Unknown error';
                if (Array.isArray(error.body)) {
                    this.error = error.body.map(e => e.message).join(', ');
                } else if (typeof error.body.message === 'string') {
                    this.error = error.body.message;
                }
               
                this.modalopen = false;
                const valueSelectedEvent = new CustomEvent('onsave', { detail: val });
                this.dispatchEvent(valueSelectedEvent);
            })

            

                }
            }
        
       
        //this.oncloseevent();
        

    oncloseevent()
    {
        const valueSelectedEvent = new CustomEvent('closemodal', { detail: false });
        this.dispatchEvent(valueSelectedEvent);
    }

    handleClick(event) {
        let searchTerm =  event.target.value;
        let objName='Inventory__c';
        console.log('search ' +event.target.value ) ;
        lookUp({searchTerm: searchTerm, myObject: objName, filter: this.filter})
        .then(result => {
            console.log('result -->',JSON.stringify(result))
            this.records = result;
           if(this.records!=null && this.records!=undefined)
           {
            let rows = JSON.parse( JSON.stringify( this.records ) );

             for ( let i = 0; i < rows.length; i++ ) {  
                rows[i].isnew = true;
             }

        this.childRecords=rows;
           }
           
        })
        .catch(error => {
            this.error = error;
        })
    }


    addRow(event)
    {
        
        
        
        
        console.log('click' + event.target.value);
        let val = this.childRecords.find(data => data.Id === event.target.value);
        
        console.log('ff ' + val.Id );
if(this.addrecords==null && this.addrecords==undefined)
{
    console.log('ff1 ' + val.Id );
    this.addrecords.push(val);
}
else
{
    console.log('ff3 ' + val.Id );
    let val1 = this.addrecords.find(data => data.Id === event.target.value|| data.Product_Name__c === event.target.value );
    if(val1==null && val1==undefined)
    {      
        console.log('ff2 ' + val.Id );
     this.addrecords.push(val);
    }
}
let val1 = this.delrecords.find(data => data.Product_Name__c === event.target.value);
    
    if(val1==null && val1==undefined)
    {
        console.log('ff4 ' + val.Id );
        if(this.newrecords==null && this.newrecords==undefined)
        {
            console.log('ff5 ' + val.Id );
            this.newrecords.push(val);
        }
        else{
            let val1 = this.newrecords.find(data => data.Product_Name__c === event.target.value||data.Id === event.target.value);
            console.log('ff6 ' + val.Id );
            if(val1==null && val1==undefined)
            {
                console.log('ff7 ' + val.Id );
                this.newrecords.push(val);
            }
        }
   
    }
    else
    {
       
        let val = this.delrecords.find(data => data.Product_Name__c === event.target.value);
        
        var index = this.delrecords.indexOf(val);
        console.log('ff8 ' + val.Id );
        if((index > -1))
        {
            console.log('ff9 ' + val.Id );
            this.delrecords.splice(index,1);
            
        } 
    }
       //this.addrecords.push({Id : val.Id,Product_Name__c : val.Product_Name__c});
      
      

       console.log('ffq ' + this.addrecords );
    //    let addrecords1 =  this.addrecords;
    //    this.addrecords = addrecords1;
    }

    deleterow(event)
    {
        let remList = [];
        remList = this.addrecords;
        let val = this.addrecords.find(data => data.Id === event.target.value);
        console.log('ff10 ' + val.Id );
        var index = remList.indexOf(val);

        if((index > -1))
        {
            console.log('ff11 ' + val.Id );
            if(val.isnew==false)
        {
            console.log('ff11 ' + val.Id );
            this.delrecords.push(val);
        }
            console.log('ff12 ' + val.Id );
            remList.splice(index,1);
            this.accRecords = remList;
        }
        
    }
}