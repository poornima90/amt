trigger EmployeeTrigger on Employee__c (after update) {
    List<Employee__c> employeeList = New List<Employee__c>();
    for(Employee__c emp : Trigger.new){
        if(emp.AMT_Active__c != true){
            employeeList.add(emp);
        }        
    }
    if(employeeList.size()>0){
    List<Asset> assetList = [select id,Product_Name__c,Active__c from Asset where Employee_Name__c IN : employeeList and Active__c = true];
    List<id> inventoryId = new List<id>();
    if(assetList.size()>0){
        for(Asset asset : assetList){
            inventoryId.add(asset.Product_Name__c);
             asset.Active__c = false;
            }                        
        }
    
      update assetList;
    }
   
}