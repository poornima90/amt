import { LightningElement, api, wire, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
//import { createRecord,getRecord} from 'lightning/uiRecordApi';
import { getPicklistValues, getObjectInfo } from 'lightning/uiObjectInfoApi';
import conMainObject from '@salesforce/schema/Asset';
import Inventory from '@salesforce/schema/Inventory__c';
import assetid from '@salesforce/schema/Asset.Id';
import employeename from '@salesforce/schema/Asset.Employee_Name__c';
import inventoryitem from '@salesforce/schema/Asset.Product_Name__c';
import Assetname from '@salesforce/schema/Asset.Name';

import serialno from '@salesforce/schema/Asset.SerialNumber';
import brand from '@salesforce/schema/Asset.Brand__c';
import active from '@salesforce/schema/Asset.Active__c';

import productFamily from '@salesforce/schema/Asset.ProductFamily';
import productCode from '@salesforce/schema/Asset.ProductCode';
import inventoryProductType from '@salesforce/schema/Asset.Inventory_Product_Type__c';

import Asset from '@salesforce/schema/Asset';
import getassest from '@salesforce/apex/AssetController.getassest';
import saveasset from '@salesforce/apex/AssetController.saveasset';
import CompanyName from '@salesforce/schema/User.CompanyName';
import options from '@salesforce/schema/Inventory__c.Product_Type__c';
import { NavigationMixin } from 'lightning/navigation';
import { CloseActionScreenEvent } from 'lightning/actions';
import id from '@salesforce/user/Id';



export default class Assestdetlwc extends NavigationMixin(LightningElement) {
    @track name = '';
    @track id = '';
    @track desig = '';
    @track typ = '';
    @track divison = '';
    @track num = '';
    @track prodname = '';
    @track prodid = '';
    @track serialno = '';
    @track family = '';
    @track prodcode = '';
    @track Brand = '';
    @track SedinAssetId = '';
    @track active = true;
    @api recordId;
    @track Assrec;
    @track error;
    @track proddtype;
    @api objectApiName;
    @track filtcrit;
    @track edit = false;
    @track isHardware = false;
    @track showSpinner = true;
    @track prdtypesel = false;
    @track empclass = 'slds-text-body_small slds-col slds-size_1-of-2  slds-form-element';
    @track prdtypeclass = 'slds-col slds-size_1-of-2';
    @track prdnameclass = 'slds-text-body_small slds-col slds-size_1-of-2';
    @track prdnmerr = false;
    @track Assetname;
    @track astnamecls = 'slds-col slds-size_1-of-2 slds-form-element';
    @track asstnmeerr;
    @track Empcountry;
    @track EmpLocation;
    @track sedinid;
    @track prodinfo;
    @track modalval=false;
    @track addrecords=[];
    @track delrecords=[];
    //slds-has-error
    emperr = false;
    prdtpeerr = false;
    @wire(getObjectInfo, { objectApiName: Inventory })
    objectInfo;

    // getRecordTypeId(recordTypeName) {
    //     let recordtypeinfo = this.objectInfo.data.recordTypeInfos;
    //     let recordTypeId;
    //     for(var eachRecordtype in  recordtypeinfo)
    //     {
    //         if(recordtypeinfo[eachRecordtype].name===recordTypeName){
    //             recordTypeId = recordtypeinfo[eachRecordtype].recordTypeId;
    //             break;
    //         }
    //     }
    //     console.log('returning -   ' + recordTypeId);
    //     return recordTypeId;
    // }    

    // @wire(getPicklistValues,
    //     {
    //         recordTypeId: '$objectInfo.data.defaultRecordTypeId', //pass id dynamically
    //         fieldApiName: options
    //     }
    // )
    @track stageValues = [
        { label: 'Laptop', value: 'Laptop' },
        { label: 'Monitor', value: 'Monitor' },
        { label: 'Desktop', value: 'Desktop' },
        { label: 'Ipad', value: 'Ipad' },
        { label: 'Mobile', value: 'Mobile' },
        { label: 'Tab', value: 'Tab' },
        { label: 'Watch', value: 'Watch' },
    ];

    @track familyValues = [
        { label: 'Hardware', value: 'Hardware' },
        { label: 'Software', value: 'Software' }
    ];

    accordianSection = ['product', 'emplo'];
    get designation() {
        return this.Assetrec.data.fields.AMT_Employee_Designation__c.value;
    }
    set designation(value) {
        this.desig = value;
    }

    get getFieldState() {
        return (this.family == undefined || this.family == '') ? true : false;
    }

    connectedCallback() {
       
        if (this.recordId != null) {
            this.edit = 'true';
            this.prdtypesel = false;
            this.getrecords();
        }
        else {
            this.active=true;
            this.showSpinner = false
            this.filtcrit = '';
            this.prdtypesel = false;
        }
    }

    handleasset(event) {
        this.astnamecls = 'slds-col slds-size_1-of-2 slds-form-element';
        this.asstnmeerr = false;
        this.Assetname = event.target.value;
    }

    getrecords() {
        getassest({ assetid: this.recordId })
            .then(result => {
                this.Assrec = result;
                this.error = undefined;
                if (this.Assrec != undefined) {
                    this.showSpinner = false;
                    this.name = this.Assrec[0].EmployeeName__c == undefined ? '' : this.Assrec[0].EmployeeName__c;
                    Assetname
                    this.Assetname = this.Assrec[0].Name == undefined ? '' : this.Assrec[0].Name;

                    this.id = this.Assrec[0].Employee_Name__c == undefined ? '' : this.Assrec[0].Employee_Name__c;
                    this.desig = this.Assrec[0].AMT_Employee_Designation__c == undefined ? '' : this.Assrec[0].AMT_Employee_Designation__c;
                    this.typ = this.Assrec[0].AMT_Employee_Type__c == undefined ? '' : this.Assrec[0].AMT_Employee_Type__c;
                    this.divison = this.Assrec[0].Employee_Business_Division__c == undefined ? '' : this.Assrec[0].Employee_Business_Division__c;
                    this.num = this.Assrec[0].Employee_No__c == undefined ? '' : this.Assrec[0].Employee_No__c;
                    this.prodname = this.Assrec[0].productName__c == undefined ? '' : this.Assrec[0].productName__c;
                    this.prodid = this.Assrec[0].Product_Name__c == undefined ? '' : this.Assrec[0].Product_Name__c;
                    this.serialno = this.Assrec[0].SerialNumber == undefined ? '' : this.Assrec[0].SerialNumber;
                    this.family = this.Assrec[0].Product_Family__c == undefined ? '' : this.Assrec[0].Product_Family__c;
                    this.prodcode = this.Assrec[0].Product_Code__c == undefined ? '' : this.Assrec[0].Product_Code__c;
                    this.Brand = this.Assrec[0].Brand__c == undefined ? '' : this.Assrec[0].Brand__c;
                    this.SedinAssetId = this.Assrec[0].Sedin_Asset_ID__c == undefined ? '' : this.Assrec[0].Sedin_Asset_ID__c;
                    this.active = this.Assrec[0].Active__c == undefined ? false : this.Assrec[0].Active__c;
                    this.proddtype = this.Assrec[0].Inventory_Product_Type__c == undefined ? '' : this.Assrec[0].Inventory_Product_Type__c;
                    this.Empcountry =this.Assrec[0].Employee_Country__c == undefined ? '' : this.Assrec[0].Employee_Country__c;
                    this.EmpLocation=this.Assrec[0].Employee_Location__c == undefined ? '' : this.Assrec[0].Employee_Location__c;
                    this.sedinid=this.Assrec[0].Asset_ID__c == undefined ? '' : this.Assrec[0].Asset_ID__c;
                    this.prodinfo=this.Assrec[0].Product_Information__c == undefined ? '' : this.Assrec[0].Product_Information__c;

               
                }
            }).catch(error => {
                this.error = 'Unknown error';
                this.showSpinner = false;
                if (Array.isArray(error.body)) {
                    this.error = error.body.map(e => e.message).join(', ');
                } else if (typeof error.body.message === 'string') {
                    this.error = error.body.message;
                }
            })
    }




    handleAccountSelection(event) {

        this.empclass = 'slds-text-body_small slds-col slds-size_1-of-2  slds-form-element';
        this.emperr = false;
        this.name = event.detail.Name;
        this.id = event.detail.Id;
        this.desig = event.detail.Designation__c;
        this.typ = event.detail.AMT_Employee_Type__c;
        this.divison = event.detail.Business_Division__c;
        this.num = event.detail.Unique_Employee_No__c;
        this.Empcountry = event.detail.Country__c;
        this.EmpLocation = event.detail.Location__c;

    }

    handleproduct(event) {
        this.prdnameclass = 'slds-text-body_small slds-col slds-size_1-of-2';
        this.prdnmerr = false;
        this.prodname = event.detail.Name;
        this.prodid = event.detail.Id;
        this.serialno = event.detail.Serial_Number__c;
        this.family = event.detail.Product_Family__c;
        this.prodcode = event.detail.Product_Code__c;
        this.Brand = event.detail.Brand__c;
        this.SedinAssetId = event.detail.Asset_ID__c;
        //this.sedinid= event.detail.Asset_ID__c;
        this.prodinfo= event.detail.Information__c;
        //this.active = event.detail.Active__c;

    }
    cancelContactScreen() {
        
    }
    handlechange(event) {
        if (event.target.name == 'active') {
            this.active = event.target.checked;
        }
    }

    handlechange1(event) {
        if (event.detail.value !== '') {
            this.prdtypeclass = 'slds-col slds-size_1-of-2';
            this.prdtpeerr = false;
            this.prdtypesel = true;
        }
        this.proddtype = event.detail.value;
        this.filtcrit = event.detail.value;
        this.prodname='';
    }

    changeProductValues(event) {
        this.family = event.detail.value;
        if(event.detail.value == 'Hardware') {
            this.isHardware = true;
            this.ser
        } else {
            this.isHardware = false;
            this.filtcrit=event.detail.value;
        }
        this.prodname='';
        this.proddtype = event.detail.value;
        this.prdtypesel = false;
    }
    clearhandle(event)
    {
        if(this.recordId==undefined)
        {
            if(event.detail=='Inventory__c' || event.detail== 'clear Inventory__c')
            {
                this.prodname='';
                this.prodid ='';
                this.serialno='';
                //this.family ='';
                this.prodcode='';
                this.Brand = '';
                this.Sedin_Asset_ID__c = '';
                //this.active ='';
                this.sedinid= '';
                this.prodinfo= '';
            }
            else if(event.detail=='Employee__c' || event.detail== 'clear Employee__c')
            {
                this.name = '';
                this.id = '';
                this.desig = '';
                this.typ = '';
                this.divison = '' ;
                this.num ='';
                this.Empcountry = '';
                this.EmpLocation = '';
            }
        }
    }

    checkerrors() {
        let errorres = false;
        if (this.recordId == null) {
            // if (this.Assetname == null || this.Assetname == undefined || this.Assetname == '') {
            //     this.astnamecls += ' slds-has-error';
            //     this.asstnmeerr = true;
            //     errorres = true;
            // }
            if (this.id == null || this.id == undefined || this.id == '') {
                this.empclass += ' slds-has-error';
                this.emperr = true;
                errorres = true;
            }

            if (this.proddtype == null || this.proddtype == undefined || this.proddtype == '') {
               
                this.prdtypeclass += ' slds-has-error';
                this.prdtpeerr = true;
                errorres = true;
            }
            if (this.prodid == null || this.prodid == undefined || this.prodid == '') {
               
                this.prdnameclass += ' slds-has-error';
                this.prdnmerr = true;
                errorres = true;
            }
        }
       
        if (errorres == false) {
            this.saverecord();
        }
    }


    handleToggleSection(event) {

    }
    saverecord() {
        this.showSpinner = true;
        let isact = true;
        if (this.recordId != null) {
            isact = false;
        }
        var fields = {};
        if (this.recordId != null) {
            fields[assetid.fieldApiName] = this.recordId;
        }
        let chl=[];
        let del=[];
        if(this.addrecords !=null  && this.addrecords !=undefined && this.addrecords.length>0 )
        {
            console.log('enter' + this.addrecords);
            if(this.addrecords[0] !=null  && this.addrecords[0] !=undefined && this.addrecords[0].length>0 )
            {
                console.log('enter1' + this.addrecords[0]);
                for ( let i = 0; i < this.addrecords[0].length; i++ ) {  
                    console.log('enter2' + this.addrecords[0][i].Id);
                    chl.push(this.addrecords[0][i]);
                    console.log('enter3' + chl);
                }
            }
            if(this.addrecords.length>1 && this.addrecords[1] !=null  && this.addrecords[1] !=undefined && this.addrecords[1].length>0 )
            {
                console.log('enter4' + this.addrecords[1]);
                for ( let i = 0; i < this.addrecords[1].length; i++ ) {  
                    console.log('enter5' + this.addrecords[1][i].Id);
                    del.push(this.addrecords[1][i].Id);
                    console.log('enter3' + del);
                }
            }


        }
        fields[employeename.fieldApiName] = this.id;
        fields[inventoryitem.fieldApiName] = this.prodid;
        fields[serialno.fieldApiName] = this.serialno;
        fields[Assetname.fieldApiName] = this.Assetname;
        fields[productFamily.fieldApiName] = this.family;
        fields[productCode.fieldApiName] = this.prodcode;
        fields[inventoryProductType.fieldApiName] = this.proddtype;
        // fields[productfamily.fieldApiName] = this.family;
        // fields[productcade.fieldApiName] = this.prodcode;
        fields[brand.fieldApiName] = this.Brand;
        fields[active.fieldApiName] = this.active;

        // const recordInput = { apiName: conMainObject.objectApiName, fields };
        saveasset({ ast: fields, isnew: isact,child: chl,delchild : del })
            .then(result => {
                if (result[0] == 'success') {
                    this.recordId = result[1];
                    // this.showSpinner=false;
                    this.dispatchEvent(
                        new ShowToastEvent({
                            title: 'Success',
                            message: 'Asset record has been created',
                            variant: 'success',
                        }),
                    );
                    this[NavigationMixin.Navigate]({
                        type: 'standard__recordPage',
                        attributes: {
                            recordId: this.recordId,
                            objectApiName: 'Asset',
                            actionName: 'view'

                        },
                    });

                }
                else {
                    this.dispatchEvent(
                        new ShowToastEvent({
                            title: 'Error',
                            message: result[0],
                            variant: 'error',
                            mode: 'dismissable'

                        }),
                    );
                }


            }).catch(error => {
                this.showSpinner = false;
                this.error = 'Unknown error';
                if (Array.isArray(error.body)) {
                    this.error = error.body.map(e => e.message).join(', ');
                } else if (typeof error.body.message === 'string') {
                    this.error = error.body.message;
                }
            })


    }
    closeModal(event) {
        this[NavigationMixin.Navigate]({
            type: 'standard__objectPage',
            attributes: {
                objectApiName: 'Asset',
                actionName: 'home',
            },
        });
    }

    handlemodal()
    {
        console.log('aqqqq1');
        this.modalval=true;
    }
    closemodal1()
    {
        console.log('aqqqq');
        this.modalval=false;
    }
    childvalues(event)
    {
        this.modalval=false;
        if(event.detail!=null && event.detail!=undefined && event.detail.length>0)
        {
            this.addrecords=event.detail;
            

        }

    }

}