public class InventoryHandler {
   @auraenabled
   public static void assetDeactivate(List<Id> inventoryIDList){
       system.debug('deactivate' + inventoryIDList);
                List<Asset> assetList = [select id,Product_Name__c,Parent_Product__c,Active__c from Asset where (((Product_Name__c in:inventoryIDList) or (Parent_Product__c=:inventoryIDList)) and Active__c=true)];
                for(Asset asset : assetList){
                    asset.Active__c = false;
                }
                update assetList;
    }
    
    private static boolean run = true;
    public static boolean runOnce()
    {
        if(run)
        {
            run=false;
            return true;
        }
        else
        {
            return run;
        }
    }
    
}