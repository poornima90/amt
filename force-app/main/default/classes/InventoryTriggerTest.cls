@isTest
private class InventoryTriggerTest {
    @isTest static void createInventory() {
        try {
            
            // Setup test data
            // Create a unique UserName
            String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
            // This code runs as the system user
            Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
            User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
                              EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                              LocaleSidKey='en_US', ProfileId = p.Id,
                              TimeZoneSidKey='America/Los_Angeles',
                              UserName=uniqueUserName);
            
            System.runAs(u) {
                
                Inventory__c inventory = new Inventory__c();
                inventory.Product_Family__c = 'Hardware';
                inventory.Product_Code__c = 'Monitor - 1';
                inventory.Division__c = 'SALESFORCE';
                inventory.Owner__c = 'SEDIN';
                inventory.Country__c = 'India';
                inventory.Location__c = 'Pune';
                inventory.Supplier_Vendor__c = 'Amazon';                
                insert inventory;
                
                Asset asst = new Asset();
                asst.Parent_Product__c = inventory.id;
                asst.SerialNumber= 'RANDIN0990';
                asst.Brand__c= 'Dell';
                asst.Active__c= true;
                insert asst;

                
                inventory.Brand__c = 'Acer';
                inventory.Retire__c =true;
                update inventory;
                
                Inventory__c softwareInventory = new Inventory__c();
                softwareInventory.Product_Family__c = 'Software';
                softwareInventory.Product_Code__c = 'MS-Office';
                softwareInventory.Division__c = 'SALESFORCE';
                softwareInventory.Owner__c = 'SEDIN';
                softwareInventory.Country__c = 'India';
                softwareInventory.Location__c = 'Pune';
                softwareInventory.Supplier_Vendor__c = 'Amazon';
                softwareInventory.License_Count__c = 2;
                
                insert softwareInventory;
                
                softwareInventory.Assigned_License_Count__c=2;
                softwareInventory.License_Count__c=1;
                update softwareInventory;
                
                softwareInventory.Assigned_License_Count__c=2;
                softwareInventory.License_Count__c=3;
                update softwareInventory;
                
                softwareInventory.Assigned_License_Count__c=1;
                softwareInventory.License_Count__c=1;
                softwareInventory.UnAssigned_License_Count__c=2;
                update softwareInventory;
                
                inventory.Brand__c = 'Dell';
                inventory.License_Count__c = 11;
                update inventory;
                
                Inventory__c inventoryList = [ SELECT Id, Name, License_Count__c FROM Inventory__c WHERE Id = : inventory.Id ];
                System.assertEquals(11, inventoryList.License_Count__c);
            }
            
        } catch (Exception e) {
            System.debug('Exception -->' +e);
        }
    }
}