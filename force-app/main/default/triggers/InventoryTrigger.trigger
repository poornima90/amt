trigger InventoryTrigger on Inventory__c (before insert,before update, after update) {
    if(Trigger.isinsert){
		for(Inventory__c inventory : Trigger.new){
            if(inventory.Product_Family__c == 'Software'){
                inventory.UnAssigned_License_Count__c = inventory.License_Count__c;
                inventory.Assigned_License_Count__c = 0;
            }
        }    
    }
    if(Trigger.isupdate){
        if(Trigger.isbefore){        
        for(Inventory__c inventory : Trigger.new){
            Inventory__c oldInventory =  Trigger.oldMap.get(inventory.id);
           
            if(inventory.Product_Family__c == 'Software'){
            Integer newInvLiCount = (Integer)inventory.License_Count__c;
            Integer oldInvLiCount = (Integer)oldInventory.License_Count__c; 
            if((newInvLiCount != oldInvLiCount) && (oldInvLiCount<newInvLiCount) ){
                integer difference =  newInvLiCount-oldInvLiCount;
                inventory.UnAssigned_License_Count__c = inventory.UnAssigned_License_Count__c + difference;
                inventory.Assignedunassigned__c = false;
            }
            else if((newInvLiCount != oldInvLiCount) && (oldInvLiCount>newInvLiCount) ){
                if((newInvLiCount>=inventory.Assigned_License_Count__c)){
                integer difference = oldInvLiCount - newInvLiCount;
                inventory.UnAssigned_License_Count__c = inventory.UnAssigned_License_Count__c - difference;
                if(inventory.UnAssigned_License_Count__c < 1)
                    inventory.Assignedunassigned__c = true;
                }
                else{
                    inventory.License_Count__c = oldInvLiCount;
                }
            }
        }
            else if(inventory.Product_Family__c == 'Hardware' && inventory.Retire__c == true){
                inventory.Active__c = false;
            }
         
        }
     }
    else{
         List<id> inventoryIDList = new List<id>();
        for(Inventory__c inventory : Trigger.new){
        Inventory__c oldInventory =  Trigger.oldMap.get(inventory.id);
            Boolean newInvRetire = inventory.Retire__c;
            Boolean oldInvRetire = oldInventory.Retire__c; 
          if(newInvRetire != oldInvRetire && newInvRetire == true ){
                
                inventoryIDList.add(inventory.Id);
         }
            
        }
        if(InventoryHandler.runOnce() && inventoryIDList.size()>0){
           InventoryHandler.assetDeactivate(inventoryIDList);
          }
    }
    }
      
}