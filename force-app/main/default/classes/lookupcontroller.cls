public with sharing class lookupcontroller {
    
    private final static Integer MAX_RESULTS = 5;
    
    
    @AuraEnabled(cacheable=true)
    public static List<sObject> search(String searchTerm, string myObject, String filter) {
        String myQuery = null;
        if(filter != null && filter != '' && myObject!='Employee__c'){
            
            myQuery = 'Select Id, Product_Name__c,Serial_Number__c,Product_Family__c,Product_Code__c,Brand__c,Active__c,Asset_ID__c,Information__c from '+myObject+ ' Where Product_Name__c Like  \'%' + searchTerm + '%\'' ;
            //  AND  Product_Type__c=  '+'\'' +filter +'\'' +' And Assignedunassigned__c= false LIMIT  5';
            
            if(filter=='Software') 
            {
                myQuery += ' AND Product_Family__c='+'\'' +filter +'\'';
                System.debug('1 --->');
            } 
            else
            {
                myQuery += ' AND Product_Family__c=\'Hardware\' AND  Product_Type__c=  '+'\'' +filter +'\'';
                System.debug('2 --->');
            }
            myQuery += ' And Assignedunassigned__c= false And Service__c=false LIMIT  5';
            
            system.debug('my query' +myQuery );
        }
        else {
            if(searchTerm == null || searchTerm == ''){
                if(myObject=='Employee__c')
                {
                    myQuery = 'Select Id, Name,Business_Division__c,Designation__c,AMT_Employee_Type__c,Unique_Employee_No__c,Location__c,Country__c from  '+myObject+' Where AMT_Active__c=true LIMIT  5';
                    System.debug('3 --->');
                    system.debug('my query' +myQuery );}
                else
                {
                    System.debug('4 --->');
                    myQuery = 'Select Id, Product_Name__c,Serial_Number__c,Product_Family__c,Product_Code__c,Brand__c,Active__c from   '+myObject+' Where LastViewedDate != NULL And Assignedunassigned__c= false ORDER BY LastViewedDate DESC LIMIT 0';
                }
            }
            else {
                if(myObject=='Employee__c')
                {
                    System.debug('5 --->');
                    myQuery = 'Select Id, Name,Business_Division__c,Designation__c,AMT_Employee_Type__c,Unique_Employee_No__c,Location__c,Country__c from  '+myObject+' Where Name Like  \'%' + searchTerm + '%\' AND  AMT_Active__c=true LIMIT  5';
                }
                else
                {
                    System.debug('6 --->');
                    myQuery = 'Select Id, Product_Name__c,Serial_Number__c,Product_Family__c,Product_Code__c,Brand__c,Active__c,Asset_ID__c,Information__c from    '+myObject+' Where Product_Name__c Like  \'%' + searchTerm + '%\' And Assignedunassigned__c= false  And Service__c=false LIMIT  5'; 
                }
            }
        }
        List<sObject> lookUpList = database.query(myQuery);
       System.debug('6 ---> ' + lookUpList );
        return lookUpList;
    }
}