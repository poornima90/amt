@isTest
public class EmployeeAgreementEmailLogTest {
    static testMethod void testCaseFromEmail() {
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        Employee__c employee= new Employee__c(Name='TestUser',AMT_Active__c=true ,Business_Division__c ='EAM',Designation__c ='Developer',Country__c='India',Location__c='Chennai',Unique_Employee_No__c='12324',AMT_Primary_Email__c = 'Test@Test.com');
        insert employee;
        Inventory__c inventory = new Inventory__c(
            Product_Family__c = 'Hardware',
            Product_Code__c = 'Monitor - 1',
            Division__c = 'SALESFORCE',
            Owner__c = 'SEDIN',
            Location__c = 'Canada',
            Supplier_Vendor__c = 'Amazon',
            License_Count__c = 10);
            
        insert inventory;
        Asset asset = new Asset(Name='TestAsset',Employee_Name__c=employee.id,Product_Name__c =inventory.id );
        insert asset;
        email.subject = 'REF-'+asset.id+'AMT## Laptop Agreement';
        email.fromAddress='Testuser@gmail.com';
        email.htmlBody='Test email';
        env.fromAddress = 'Testuser@gmail.com';
        EmployeeAgreementEmailLog emailHandler = new EmployeeAgreementEmailLog();
        emailHandler.handleInboundEmail(email, env);
        email.subject = 'REF-'+asset.id+'AMT Laptop Agreement';
        email.htmlBody='accepted';
        emailHandler.handleInboundEmail(email, env);        
    }
    
}