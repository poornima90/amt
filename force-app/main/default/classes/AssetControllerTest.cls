@isTest
public class AssetControllerTest {
@isTest static void assetsaveandget()
{
     try {
       String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
            // This code runs as the system user
            Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
            User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
                              EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                              LocaleSidKey='en_US', ProfileId = p.Id,
                              TimeZoneSidKey='America/Los_Angeles',
                              UserName=uniqueUserName);
            
            System.runAs(u) {
 		Employee__c emp = new Employee__c();
        emp.Name = 'Test';
        emp.Unique_Employee_No__c = 'Test123';
        emp.Business_Division__c ='EAM';
        emp.Designation__c = 'Technical Consultant';
        emp.Country__c = 'India';
        emp.Location__c = 'Chennai';
        emp.AMT_Primary_Email__c = 'Test@test.com';
        emp.AMT_Active__c = true;
        insert emp;
         Inventory__c inv = new Inventory__c();
         inv.Product_Family__c	='Hardware';
         inv.Product_Name__c='Test';
         inv.Product_Type__c='Laptop';
         inv.Product_Code__c='Laptop';
         inv.Division__c='TARKA LABS';
         inv.Location__c='India';
         inv.Owner__c='SEDIN';
         inv.Supplier_Vendor__c='Croma';
         insert inv;
     			Asset asst  = new Asset();
              asst.Name=  'Test'; // 'testforone -'+ ast.Product_Name__c +  ast.Employee_Name__c + system.Datetime.now();
              asst.Employee_Name__c= emp.Id;
             // asst.Product_Name__c = 'test';
              asst.Inventory__c = inv.id;
    insert asst;
    AssetController.getassest(asst.AccountId);
   			 Asset asst1  = new Asset();
              asst1.Name=  'Test1'; // 'testforone -'+ ast.Product_Name__c +  ast.Employee_Name__c + system.Datetime.now();
              asst1.Employee_Name__c= emp.Id;
             // asst.Product_Name__c = 'test';
              asst1.Inventory__c = inv.id;
    		AssetController.saveasset(asst1, true);
    		AssetController.saveasset(asst1, false);
    		AssetController.getinvdet(inv.Id);
    		AssetController.getexppirylst();
            }
}
    catch (Exception e) {
            System.debug('Exception -->' +e);
        }
    
}
}